from PyQt5.QtWidgets import QWidget, QMessageBox, QApplication,QGridLayout,QLabel,QCheckBox,QTextEdit,QPushButton,QMainWindow
from PyQt5.QtCore import Qt
from camera import *

class setNtp(object):
    def setupNtp(self, MainWindow):
        MainWindow.setWindowTitle("configure NTP address")
        self.centralwidget = QWidget(MainWindow)

        grid = QGridLayout(self.centralwidget)

        MainWindow.setCentralWidget(self.centralwidget)

        self.parameter = MainWindow.parameter

        lblDummy1= QLabel(" ")
        grid.addWidget(lblDummy1,0,0,1,1)

        lblDummy2 = QLabel(" ")
        grid.addWidget(lblDummy2, 1, 0, 1, 1)

        self.ntpDhcp = QCheckBox("NTP assigned from DHCP")
        self.ntpDhcp.stateChanged.connect(self.changeNtp)
        grid.addWidget(self.ntpDhcp,2,0,1,1)

        lblDummy3 = QLabel(" ")
        grid.addWidget(lblDummy3, 3, 0, 1, 1)

        lblNtp1= QLabel("NTP1")
        grid.addWidget(lblNtp1,4,0,1,-1)

        self.textNtp1= QTextEdit(" ")
        self.textNtp1.setMaximumHeight(lblNtp1.sizeHint().height() * 2)
        self.textNtp1.setTabChangesFocus(True)
        self.textNtp1.setAlignment(Qt.AlignCenter)
        self.textNtp1.focusInEvent = lambda _: self.textNtp1.selectAll()
        self.textNtp1.mousePressEvent = lambda _: self.textNtp1.selectAll()
        grid.addWidget(self.textNtp1,4,1,1,-1)

        lblDummy4 = QLabel(" ")
        grid.addWidget(lblDummy4, 5, 0, 1, 1)

        lblNtp2 = QLabel("NTP2")
        grid.addWidget(lblNtp2, 6, 0, 1, -1)

        self.textNtp2 = QTextEdit(" ")
        self.textNtp2.setMaximumHeight(lblNtp2.sizeHint().height() * 2)
        self.textNtp2.setTabChangesFocus(True)
        self.textNtp2.setAlignment(Qt.AlignCenter)
        self.textNtp2.focusInEvent = lambda _: self.textNtp2.selectAll()
        self.textNtp2.mousePressEvent = lambda _: self.textNtp2.selectAll()
        grid.addWidget(self.textNtp2, 6, 1, 1, -1)

        lblDummy5 = QLabel(" ")
        grid.addWidget(lblDummy5, 7, 0, 1, 1)


        self.btnApply = QPushButton("applica modifiche")
        grid.addWidget(self.btnApply, 8, 1, 1, 1)
        #self.btnApply.clicked.connect(self.applyConf)

        lblDummy6 = QLabel(" ")
        grid.addWidget(lblDummy6, 9, 0, 1, 1)

        self.btnJumpOver6 = QPushButton("Next")
        self.btnJumpOver6.setAutoDefault(True)
        grid.addWidget(self.btnJumpOver6, 10, 1, 1, 1)

        self.cam()
        self.readData()

    def cam(self):
        cam=self.parameter['ip']
        user=self.parameter['user']
        pwd=self.parameter['pass']
        port=self.parameter['port']

        try:
            self.new_camera = Camera(cam, port, user, pwd)
            capability = self.new_camera.GetCapability()            # to check if the username and password are correct we must ask some data to the camera
        except:
            QMessageBox.critical(self.centralwidget, "Error",
                                                     "check username and password, quitting")
            quit()

    def readData(self):
        result=self.new_camera.GetNtp()
        if result['ntpDhcp']:
            self.ntpDhcp.setChecked(True)
            self.textNtp1.setDisabled(True)
            self.textNtp2.setDisabled(True)
        else:
            self.ntpDhcp.setChecked(False)
            self.textNtp1.setDisabled(False)
            self.textNtp2.setDisabled(False)


    def changeNtp(self):
        if self.ntpDhcp.isChecked():
            self.textNtp1.setDisabled(True)
            self.textNtp2.setDisabled(True)
        else:
            self.textNtp1.setDisabled(False)
            self.textNtp2.setDisabled(False)
