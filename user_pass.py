from PyQt5.QtWidgets import QWidget, QMessageBox, QApplication,QGridLayout,QLabel,QCheckBox,QTextEdit,QPushButton,QMainWindow
from PyQt5.QtCore import Qt

class userPass(object):
    def setup_userPass(self, MainWindow):
        MainWindow.setWindowTitle("Configure onvif user and password")
        self.centralwidget = QWidget(MainWindow)
        grid = QGridLayout(self.centralwidget)
        MainWindow.setCentralWidget(self.centralwidget)

        lblUsername = QLabel("Username")
        lblUsername.setAlignment(Qt.AlignCenter)
        grid.addWidget(lblUsername, 0, 0, 1, 1)

        self.userText = QTextEdit("nome")
        self.userText.setAlignment(Qt.AlignCenter)
        self.userText.setTabChangesFocus(True)
        self.userText.focusInEvent = lambda _: self.userText.selectAll()
        self.userText.mousePressEvent = lambda _: self.userText.selectAll()
        grid.addWidget(self.userText, 0, 1, 1, -1)

        lblPass = QLabel("Password ")
        lblPass.setAlignment(Qt.AlignCenter)
        grid.addWidget(lblPass, 1, 0, 1, 1)

        self.passText = QTextEdit("Password")
        self.passText.setAlignment(Qt.AlignCenter)
        self.passText.setTabChangesFocus(True)
        self.passText.focusInEvent = lambda _: self.passText.selectAll()
        self.passText.mousePressEvent = lambda _: self.passText.selectAll()
        grid.addWidget(self.passText, 1, 1, 1, -1)

        self.btnJumpOver4 = QPushButton("Next")
        self.btnJumpOver4.setAutoDefault(True)
        grid.addWidget(self.btnJumpOver4,10,1)


