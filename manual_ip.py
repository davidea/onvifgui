from PyQt5.QtWidgets import QWidget, QMessageBox, QApplication,QGridLayout,QLabel,QCheckBox,QTextEdit,QPushButton,QMainWindow,QListWidget

from PyQt5.QtCore import Qt

class manualIp(object):
    def setup_manual(self, MainWindow):
        MainWindow.setWindowTitle("Manual insertion")
        self.centralwidget = QWidget(MainWindow)
        grid = QGridLayout(self.centralwidget)
        MainWindow.setCentralWidget(self.centralwidget)

        lbl_empty1 = QLabel("")
        lbl_empty2 = QLabel("")
        grid.addWidget(lbl_empty1, 0, 0)
        grid.addWidget(lbl_empty2, 1, 0)

        lblip = QLabel("IP Address: ")
        lblip.setAlignment(Qt.AlignCenter)
        grid.addWidget(lblip, 2, 0, 1, 1)

        self.ipText = QTextEdit("ip")
        self.ipText.setAlignment(Qt.AlignCenter)
        self.ipText.setTabChangesFocus(True)
        self.ipText.focusInEvent = lambda _: self.ipText.selectAll()        #select the text in the editbox when focus with tab key
        self.ipText.mousePressEvent=lambda _: self.ipText.selectAll()       #select the text in the editbox when focus with a click
        #self.ipText.setDisabled(True)
        grid.addWidget(self.ipText, 2, 1, 1, 1)

        lbl_empty3 = QLabel("")
        lbl_empty4 = QLabel("")
        grid.addWidget(lbl_empty3, 3, 0)
        grid.addWidget(lbl_empty4, 4, 0)

        lblPort = QLabel("onvif Port ")
        lblPort.setAlignment(Qt.AlignCenter)
        grid.addWidget(lblPort, 5, 0, 1, 1)

        self.portText = QTextEdit("port")
        self.portText.setTabChangesFocus(True)
        self.portText.setAlignment(Qt.AlignCenter)
        self.portText.focusInEvent = lambda _: self.portText.selectAll()
        self.portText.mousePressEvent=lambda _: self.portText.selectAll()
        grid.addWidget(self.portText, 5, 1, 1, 1)

        lbl_empty5 = QLabel("")
        lbl_empty6 = QLabel("")
        grid.addWidget(lbl_empty5, 6, 0)
        grid.addWidget(lbl_empty6, 7, 0)

        self.btnJumpOver3 = QPushButton("Next")
        self.btnJumpOver3.setAutoDefault(True)                              #with the enter key we simulate a click button
        grid.addWidget(self.btnJumpOver3,8,1)


