#! /usr/bin/python3
from PyQt5.QtWidgets import QMessageBox
from splash_screen import *
from discovery import *
from manual_ip import *
from user_pass import *
from set_ip import *
from set_ntp import *
from link  import *

import sys

'''
This is the entry point , here we declare the Mainwindow , it is a frame where we put our window:

    splash_screen.py            first windows to introduce to the onvifgui project and some info
    discovery.py                a window to discover a onvif device , or insert manually the ip address
                                in this window if we press the discovery button we discover the wds device in our network 
                                including the ip onvif camera that have enabled the discovery procedure
                                or if we hit the manual ip address button we step into the manual ip window
    manual_ip.py                to set manually the camera's ip address which we connect
    user_pass.py                to set the user and password for onvif connection
    set_ip.py                   where we read the ip configuration trought the onvif protocol and we can change it
    link.py                     to display the link for snapshot and media streaming
 



'''

class MainWindow(QMainWindow):
    def __init__(self, parent=None):

        super(MainWindow, self).__init__(parent)
        self.parameter = {}
        self.setGeometry(50, 50, 400, 450)
        self.intro = splashScreen()                         #first step , we create an object from the class
        self.discovery = discoveryScreen()
        self.manual = manualIp()
        self.user = userPass()
        self.setip = setIp()
        self.ntp =setNtp()
        self.link = link()

        self.startSplash()                                  #third step , display our window , now the control is in our window

    def startSplash(self):                                  #second step , declare the start function about our  window
        self.intro.setup_splash(self)
        self.intro.btnJumpOver.clicked.connect(self.startDiscovery)   #from the splash screen, if we hit the next button (btnJumpOver) we call the next window start function
        self.show()

    def startDiscovery(self):
        self.discovery.setup_discovery(self)
        self.discovery.inserisci.clicked.connect(self.startManual)
        self.discovery.btnJumpOver2.clicked.connect(self.discoveryComplete)
        self.show()

    def startManual(self):
        self.manual.setup_manual(self)
        self.manual.btnJumpOver3.clicked.connect(self.choice)
        self.show()

    def startUser(self):
        self.user.setup_userPass(self)
        self.user.btnJumpOver4.clicked.connect(self.addUser)

    def startSetIp(self):
        self.setip.setup_setip(self)
        self.setip.btnJumpOver5.clicked.connect(self.configureIpComplete)

    def startNtp(self):
        self.ntp.setupNtp(self)


    def startLink(self):
        self.link.setupLink(self)





    def verify(self,ip):
        how_dot=ip.count(".")
        if how_dot!=3:
            print("3 dot missing")
            return False

        number=ip.split(".")
        if (int(number[0])< 256 and int(number[1])< 256 and int(number[2])< 256 and int(number[3])< 256 ):
            print("ip address ok")
            return True
        else:
            print("error")
            return False


    def choice(self):
        ip_ok=0
        port_ok=0
        if self.verify(self.manual.ipText.toPlainText().strip()):
            self.parameter['ip']=self.manual.ipText.toPlainText().strip()
            ip_ok=1
        else:
            self.manual.error = QMessageBox.critical(self.manual.centralwidget, "Error",
                                                         "An error occurred in your ip address")

        try:
            port=int(self.manual.portText.toPlainText().strip())
            if (port >0 and port < 65535):
                self.parameter['port']=port
                port_ok=1
            else:
                self.manual.error = QMessageBox.critical(self.manual.centralwidget, "Error",
                                                         "An error occurred in your port number")
        except:
            self.manual.error = QMessageBox.critical(self.manual.centralwidget, "Error",
                                                     "An error occurred in your port number it must be an integer")
        if ip_ok and port_ok :
            self.startUser()

    def addUser(self):
        self.parameter['user']=self.user.userText.toPlainText().strip()
        self.parameter['pass']=self.user.passText.toPlainText().strip()
        self.startSetIp()

    def discoveryComplete(self):
        try:
            self.discovery.parameter['ip']
            self.parameter=self.discovery.parameter
            self.startUser()
        except:
            self.discovery.error = QMessageBox.critical(self.discovery.centralwidget, "Error",
                                                     "you must select a camera from the list")


    def configureIpComplete(self):
        print (self.setip.parameter)
        self.startNtp()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MainWindow()
    sys.exit(app.exec_())
