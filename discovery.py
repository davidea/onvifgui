from PyQt5.QtWidgets import QWidget, QMessageBox, QApplication,QGridLayout,QLabel,QCheckBox,QTextEdit,QPushButton,QMainWindow,QListWidget
from PyQt5.QtCore import Qt
import wsdiscovery

class discoveryScreen(object):
    def setup_discovery(self, MainWindow):
        self.parameter={}                                           # create a dictionary, we use it to extract the ip port name and hw from this windows
                                                                    # accessing it from onvifguy.py
        MainWindow.setWindowTitle("Discovery camera")
        self.centralwidget = QWidget(MainWindow)
        grid = QGridLayout(self.centralwidget)
        MainWindow.setCentralWidget(self.centralwidget)

        self.elenco = []                                            # definizione di elenco , verrà usato dalla funzione di ricerca dispositivi wds per
                                                                    # restituire l' elenco dei dispositivi trovati con ip, port, name e  hw

        self.ricerca = QPushButton(
            "cerca dispositivi onvif")                              # da qui' inizia la definizione dei pulsanti, elenchi e quant' altro dovremo
                                                                    # disporre sulla nostra interfaccia
        self.ricerca.clicked.connect(self.find)                     # eventualmente connettere il signal dell' oggetto all' azione da compiere
        self.ricerca.setAutoDefault(True)
        # che è una funzione descritta sotto
        grid.addWidget(self.ricerca, 0, 0, 1, -1)  # quindi posizionare l' oggetto sul nostro widget grid
        # dove x,y,z,w indicano x = row , y = col , z .... w ......

        self.inserisci = QPushButton("inserisci manualmente indirizzo ip")

        self.inserisci.setAutoDefault(True)
        grid.addWidget(self.inserisci, 1, 0, 1, -1)


        self.lista = QListWidget()
        grid.addWidget(self.lista, 2, 0, 1, -1)
        self.lista.clicked.connect(self.scelta)


        self.btnJumpOver2 = QPushButton("Next")                 # we create the button to jump in the next window
                                                                # this button is the only that relay is action on the
                                                                # startsplash function in the onvifgui.py file
        grid.addWidget(self.btnJumpOver2,10,1)


    def find(self):
        if (self.lista.count() > 0):

            for i in range((self.lista.count() - 1), -1, -1):
                self.lista.takeItem(i)

        wsd = wsdiscovery.WSDiscovery()
        wsd.start()
        ret = wsd.searchServices()
        for service in ret:
            port = ""
            uuid = str(service.getEPR()).split("uuid:")[1]

            address = str(service.getXAddrs()[0]).split("onvif")

            ip_address = address[0].split("http://")
            ip_address = ip_address[1].split("/")[0].strip()
            if ":" in ip_address:
                port = ip_address.split(":")[1]
                ip_address = ip_address.split(":")[0].strip()

            else:
                port = "80"

            name = ""
            hardware = ""
            for linea in service._scopes:
                line = str(linea)
                if "name" in line:
                    name = line.split("name/")[1]
                if "hardware" in line:
                    hardware = line.split("hardware/")[1]

            ip_address = ip_address + ((17 - len(ip_address)) * " ")
            port = port + ((6 - len(port)) * " ")
            self.lista.addItem(ip_address + " " + port)
            self.elenco.append({"ip": ip_address, "port": port, "uuid": uuid, "name": name, "hw": hardware})

        wsd.stop()
        self.lista.setMinimumWidth(self.lista.sizeHintForColumn(0) + 10)


    def scelta(self):

        a = self.lista.currentItem().text().split(" ")[0]
        for camera in self.elenco:
            if a in camera['ip']:
                self.parameter['ip'] = camera['ip'].strip()
                self.parameter['port'] = camera['port'].strip()
                self.parameter['name'] = camera['name'].strip()
                self.parameter['hw'] = camera['hw'].strip()
