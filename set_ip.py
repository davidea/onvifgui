from PyQt5.QtWidgets import QWidget, QMessageBox, QApplication,QGridLayout,QLabel,QCheckBox,QTextEdit,QPushButton,QMainWindow
from PyQt5.QtCore import Qt
from camera import *


class setIp(object):
    def setup_setip(self, MainWindow):
        MainWindow.setWindowTitle("Configure ip address")
        self.error=0
        self.MainWindow=MainWindow                                  # copy the MainWindows to enable this script to call the start_discovery window
                                                                    # becouse in some router when we set the dhcp on , the router assign another ip address
                                                                    # then we must rediscover it again
                                                                    # after a ip change there will be some second delay , we use a dialog to hide it!!
        self.parameter=MainWindow.parameter                         # copy inside the script the parameter from the main window , after we finish and hit the next
                                                                    # button we read from inside the MainWindow the self.parameter (self.setip.parameter[])
        self.centralwidget = QWidget(MainWindow)
        grid = QGridLayout(self.centralwidget)
        MainWindow.setCentralWidget(self.centralwidget)



        self.chkDhcp = QCheckBox("Dhcp")
        self.chkDhcp.stateChanged.connect(self.changeDhcp)
        grid.addWidget(self.chkDhcp, 0, 0)

        lblip = QLabel("IP Address: ")
        grid.addWidget(lblip, 1, 0,1,-1)

        self.ipText = QTextEdit("ip")
        self.ipText.setMaximumHeight(lblip.sizeHint().height() * 2)
        self.ipText.setTabChangesFocus(True)
        self.ipText.setAlignment(Qt.AlignCenter)
        self.ipText.focusInEvent = lambda _: self.ipText.selectAll()
        self.ipText.mousePressEvent = lambda _: self.ipText.selectAll()

        grid.addWidget(self.ipText, 1, 1,1,-1)

        lblNet = QLabel("Netmask: ")
        grid.addWidget(lblNet, 2, 0)

        self.netText = QTextEdit("  ")
        self.netText.setMaximumHeight(lblNet.sizeHint().height() * 2)
        self.netText.setTabChangesFocus(True)
        self.netText.setAlignment(Qt.AlignCenter)
        self.netText.focusInEvent = lambda _: self.netText.selectAll()
        self.netText.mousePressEvent = lambda _: self.netText.selectAll()

        grid.addWidget(self.netText, 2, 1,1,-1)

        lblGw = QLabel("GW Address: ")
        grid.addWidget(lblGw, 3, 0,1,-1)

        self.gwText = QTextEdit("  ")
        self.gwText.setMaximumHeight(lblGw.sizeHint().height() * 2)
        self.gwText.setTabChangesFocus(True)
        self.gwText.setAlignment(Qt.AlignCenter)
        self.gwText.focusInEvent = lambda _: self.gwText.selectAll()
        self.gwText.mousePressEvent = lambda _: self.gwText.selectAll()
        grid.addWidget(self.gwText, 3, 1,1,-1)

        self.chkDns= QCheckBox("DNS from DHCP")
        self.chkDns.stateChanged.connect(self.changeDns)
        grid.addWidget(self.chkDns,5,0)

        lblDns1 = QLabel("DNS1 Address: ")
        grid.addWidget(lblDns1, 6, 0,1,-1)

        self.Dns1Text = QTextEdit(" ")
        self.Dns1Text.setMaximumHeight(lblDns1.sizeHint().height() * 2)
        self.Dns1Text.setTabChangesFocus(True)
        self.Dns1Text.setAlignment(Qt.AlignCenter)
        self.Dns1Text.focusInEvent = lambda _: self.Dns1Text.selectAll()
        self.Dns1Text.mousePressEvent = lambda _: self.Dns1Text.selectAll()
        grid.addWidget(self.Dns1Text, 6, 1,1,-1)

        lblDns2 = QLabel("DNS2 Address: ")
        grid.addWidget(lblDns2, 7, 0,1,-1)

        self.Dns2Text = QTextEdit(" ")
        self.Dns2Text.setMaximumHeight(lblDns2.sizeHint().height() * 2)
        self.Dns2Text.setTabChangesFocus(True)
        self.Dns2Text.setAlignment(Qt.AlignCenter)
        self.Dns2Text.focusInEvent = lambda _: self.Dns2Text.selectAll()
        self.Dns2Text.mousePressEvent = lambda _: self.Dns2Text.selectAll()
        grid.addWidget(self.Dns2Text, 7, 1,1,-1)

        lblOnvifPort=QLabel("Onvif Port")
        grid.addWidget(lblOnvifPort,8,0,1,-1)

        self.OnvifPortText=QTextEdit(" ")
        self.OnvifPortText.setMaximumHeight(lblDns2.sizeHint().height() * 2)
        #self.OnvifPortText.setFixedWidth(140)
        grid.addWidget(self.OnvifPortText,8,1,1,-1)

        self.httpPort=QLabel("porta http = ")
        grid.addWidget(self.httpPort,9,0,1,-1)

        self.rtspPort=QLabel("porta RTSP = ")
        grid.addWidget(self.rtspPort,9,1,1,-1)

        self.ifName=QLabel("eth0")
        grid.addWidget(self.ifName,10,0,1,-1)

        self.macAddress=QLabel("00:11:22:33:44:55")
        grid.addWidget(self.macAddress,10,1,1,-1)

        self.btnReread=QPushButton("rileggi conf")
        grid.addWidget(self.btnReread,11,0,1,1)
        self.btnReread.clicked.connect(self.readConf)

        self.btnApply=QPushButton("applica modifiche")
        grid.addWidget(self.btnApply,11,1,1,1)
        self.btnApply.clicked.connect(self.applyConf)



        self.btnJumpOver5 = QPushButton("Next")
        self.btnJumpOver5.setAutoDefault(True)
        grid.addWidget(self.btnJumpOver5,12,1)

        self.cam()                                          # read the cam actual configuration

    def changeDhcp(self):                                   # when we hit the Dhcp checkbox, we change the editable status of the textedit box

        if self.chkDhcp.isChecked():
            self.ipText.setDisabled(True)
            self.netText.setDisabled(True)
            self.gwText.setDisabled(True)
        else:
            self.ipText.setDisabled(False)
            self.netText.setDisabled(False)
            self.gwText.setDisabled(False)


    def changeDns(self):                                    # same as above
        if self.chkDns.isChecked():
            self.Dns1Text.setDisabled(True)
            self.Dns2Text.setDisabled(True)
        else:
            self.Dns1Text.setDisabled(False)
            self.Dns2Text.setDisabled(False)

    def readConf(self):                                     # when we hit the read conf button , we recall the function that read the actual ip cam configuration
        self.cam()

    def applyConf(self):                                    # when we hit the button to apply the configuration , we fisrt check if the configurationi is changed

        dhcp = self.chkDhcp.isChecked()                     # we read the actual parameter textbox and checkbox
        if self.MainWindow.verify(self.ipText.toPlainText().strip()):
            new_ip = self.ipText.toPlainText().strip()
        else:
            self.error=1

        if self.MainWindow.verify(self.netText.toPlainText().strip()):
            new_mask = self.netText.toPlainText().strip()
        else:
            self.error = 1

        if self.MainWindow.verify(self.gwText.toPlainText().strip()):
            new_gw = self.gwText.toPlainText().strip()
        else:
            self.error = 1

        if self.error:
            QMessageBox.critical(self.centralwidget, "Error",
                                         "An error occurred in your ip/netmask/gateway address")
            self.error=0
            return

        if self.parameter['dhcp'] != dhcp:                  # if we change the DHCP configuration
            if dhcp:                                        # when enabled . we change it in the camera
                ris=self.new_camera.SetDhcpOn()
                self.parameter['dhcp']=True
                QMessageBox.information(self.centralwidget, "Attention",
                                                         "the camera is changing it's ip address,\n"
                                                         "if you changed the dns configuration too\n"
                                                         "it will be lost.\n"
                                                         "please rescan the network",buttons=QMessageBox.Ok)
                self.MainWindow.startDiscovery()            # and then we jump to the discovery window , because when enabled the dhcp the camera change his ip address
            else:
                if (self.error == 0):
                    self.new_camera.SetIpAddress(new_ip,new_mask,new_gw)        # if we disable the dhcp, setting manually the ip address , we change it and
                                                                                # rerun the discovery window
                    self.parameter['dhcp'] = False
                    self.parameter['ip']=new_ip
                    self.parameter['netmask']=new_mask
                    self.parameter['gw']=new_gw
                QMessageBox.information(self.centralwidget, "Attention",
                                                     "the camera is changing it's ip address,\n"
                                                         "if you changed the dns configuration too\n"
                                                         "it will be lost.\n"
                                                         "please rescan the network",buttons=QMessageBox.Ok)
                self.MainWindow.startDiscovery()


        if(new_ip != self.parameter['ip'] or new_mask != self.parameter['netmask'] or new_gw != self.parameter['gw']):
            print("conf rete cambiata")
            self.new_camera.SetIpAddress(new_ip,new_mask,new_gw)
            self.parameter['ip'] = new_ip
            self.parameter['netmask'] = new_mask
            self.parameter['gw'] = new_gw
            QMessageBox.information(self.centralwidget, "Attention",
                                    "the camera is changing it's ip address,\n"
                                    "if you changed the dns configuration too\n"
                                    "it will be lost.\n"
                                    "please rescan the network", buttons=QMessageBox.Ok)
            self.MainWindow.startDiscovery()

        new_dns_auto=self.chkDns.isChecked()
        if new_dns_auto != self.parameter['dnsAuto']:
            print("dns auto cambiato")
            if new_dns_auto:
                self.new_camera.SetDnsAuto()
                self.parameter['dnsAuto']=True
                print("dns configurato da dhcp")
            else:
                if self.MainWindow.verify(self.Dns1Text.toPlainText().strip()):
                    new_dns1 = self.Dns1Text.toPlainText().strip()
                else:
                    self.error=1
                if self.MainWindow.verify(self.Dns2Text.toPlainText().strip()):
                    new_dns2 = self.Dns2Text.toPlainText().strip()
                else:
                    self.error=1
                if self.error == 0:
                    print("dns cambiati e configurati manualmente")
                    self.new_camera.SetDnsManual(new_dns1,new_dns2)
                    self.parameter['dns1']=new_dns1
                    self.parameter['dns2']=new_dns2
                else:
                    QMessageBox.critical(self.centralwidget, "Error",
                                         "An error occurred in your dns address")
                    self.error=0
                    return

    def cam(self):
        cam=self.parameter['ip']
        user=self.parameter['user']
        pwd=self.parameter['pass']
        port=self.parameter['port']

        try:
            self.new_camera = Camera(cam, port, user, pwd)
            configurazione = self.new_camera.GetNetworkInfo()
        except:
            QMessageBox.critical(self.centralwidget, "Error",
                                                     "check username and password, quitting")
            quit()
        if(configurazione['dhcp']):
            self.chkDhcp.setChecked(True)
            self.ipText.setDisabled(True)
            self.netText.setDisabled(True)
            self.gwText.setDisabled(True)
            self.parameter['dhcp']=True
        else:
            self.parameter['dhcp'] = False
            self.chkDhcp.setChecked(False)
            self.ipText.setDisabled(False)
            self.netText.setDisabled(False)
            self.gwText.setDisabled(False)

        self.ipText.setText(cam)
        self.netText.setText(configurazione['netmask'])
        self.parameter['netmask']=configurazione['netmask']
        self.gwText.setText(configurazione['gw'])
        self.parameter['gw'] = configurazione['gw']
        self.httpPort.setText("porta http = "+configurazione['http'])
        self.parameter['http'] = configurazione['http']
        self.rtspPort.setText("porta RTSP = "+configurazione['rtsp'])
        self.parameter['rtsp'] = configurazione['rtsp']
        self.ifName.setText(configurazione['ifName'])
        self.parameter['ifName'] = configurazione['ifName']
        self.macAddress.setText(configurazione['mac'])
        self.parameter['mac'] = configurazione['mac']
        confDns=self.new_camera.GetDns()
        self.Dns1Text.setText((confDns['dns1']))
        self.parameter['dns1'] = confDns['dns1']
        self.Dns2Text.setText((confDns['dns2']))
        self.parameter['dns2'] = confDns['dns2']
        self.OnvifPortText.setText(port)
        self.chkDns.setChecked(confDns['dhcp'])
        self.parameter['dnsAuto'] = confDns['dhcp']
        if(confDns['dhcp']):
            self.Dns1Text.setDisabled(True)
            self.Dns2Text.setDisabled(True)

        #new_camera.GetDns()
        self.OnvifPortText.setDisabled(True)


