from PyQt5.QtWidgets import QWidget, QMessageBox, QApplication,QGridLayout,QLabel,QCheckBox,QTextEdit,QPushButton,QMainWindow
from PyQt5.QtCore import Qt

class splashScreen(object):
    def setup_splash(self, MainWindow):
        MainWindow.setWindowTitle("Onvifgui intro")             # optional we add a title to the window
        self.centralwidget = QWidget(MainWindow)                # we create a central widget, why?
                                                                # the Mainwindow from top to bottom have: menubar , menu icon ,
                                                                # a widget area ,and  a status bar
        grid = QGridLayout(self.centralwidget)                  # i have decided to insert inside the widget area a grid layout to
                                                                # better place the checkbox, label, textedit and more on the widget area
        MainWindow.setCentralWidget(self.centralwidget)         # assign to the window the widget that we create


#now we have a blank sheet where we can design and place our object, under this line we declare it!!!

        info_text ="The onvifgui app , born from an idea by davidea\n" \
                   "\n" \
                   "a tool that can by the onvif protocol manage the camera\n" \
                   "and setup it \n\n" \
                   "at this stage we can:\n" \
                   "discover the onvif camera on our network\n" \
                   "connect to a dicovered camera on a camera for wich we\n" \
                   "provide the ip address\n\n" \
                   "read the setting about ip address, dns , net and if we will\n" \
                   "change it"

        lblinfo = QLabel(info_text)
        lblinfo.setAlignment(Qt.AlignCenter)
        grid.addWidget(lblinfo, 0, 0,1,-1)                       #place the label in the grid , culumn 0 , row 0

        self.btnJumpOver = QPushButton("Next")                  # we create the button to jump in the next window
                                                                # this button is the only that relay is action on the
                                                                # startsplash function in the onvifgui.py file
        self.btnJumpOver.setAutoDefault(True)
        grid.addWidget(self.btnJumpOver,10,1)

